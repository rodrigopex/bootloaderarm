# Projeto BootLoader ARM #

Para que você consigo compilar e executar o projeto, você deve instalar algumas ferramentas e seguir um passo-a-passo. Supondo que você está usando Linux.

### Configuração ###

* Instalar os pacotes: gdb-arm-none-eabi, gcc-arm-none-eabi
```
$ sudo aptitude install gdb-arm-none-eabi gcc-arm-none-eabi
```
* Instalar o QEMU para ARM [GNU ARM Eclipse project - QEMU](http://gnuarmeclipse.github.io/qemu/) 
    * [Instalador 32 bits](https://github.com/gnuarmeclipse/qemu/releases/download/gae-2.4.50-20151029/gnuarmeclipse-qemu-debian32-2.4.50-201510290935-dev.tgz)
    * [Instalador 64 bits](https://github.com/gnuarmeclipse/qemu/releases/download/gae-2.4.50-20151029/gnuarmeclipse-qemu-debian64-2.4.50-201510290935-dev.tgz)
* Edite o Makefile dentro do pasta do projeto ajustando o caminho das ferramentas instaladas.

### Compilando o código ###
Baixe o [código do projeto](https://bitbucket.org/rodrigopex/bootloaderarm/downloads). Descompacte e entre na pasta. Use os comandos:

* Compilar:
```
#!shell
$ make
```

* Iniciar a sessão do QEMU:
```
#!shell
$ make run
```

* Para depurar o código você deve executar em outro terminal:
```
$ <CAMINHO_PARA_O_GDB_ARM>/arm-none-eabi-gdb -tui test.elf
GNU gdb (GNU Tools for ARM Embedded Processors) 7.10.1.20160210-cvs
Copyright (C) 2015 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "--host=x86_64-apple-darwin10 --target=arm-none-eabi".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from test.elf...done.
(gdb) target remote localhost:1234
Remote debugging using localhost:1234
0x00000000 in ?? ()
(gdb) load 
Loading section .startup, size 0x10 lma 0x10000
Loading section .text, size 0x1c lma 0x10010
Start address 0x10000, load size 44
Transfer rate: 352 bits in <1 sec, 22 bytes/write.
(gdb)
```

* Para depurar use os comandos do gdb. [Tutorial](http://www.yolinux.com/TUTORIALS/GDB-Commands.html).

* Apagar arquivos gerados:
```
#!shell
$ make clean
```