ARM_TOOLCHAIN=/Applications/yotta.app/Contents/Resources/prerequisites/gcc-arm-none-eabi-4_9-2015q3/bin
C_FLAGS=-Wall -mcpu=cortex-m4 -mthumb -mfloat-abi=softfp -mfpu=fpv4-sp-d16
AS_FLAGS=${C_FLAGS} -EL
QEMU="/Applications/GNU ARM Eclipse/QEMU/2.4.50-201510290935-dev/bin/qemu-system-gnuarmeclipse"
all:
	${ARM_TOOLCHAIN}/arm-none-eabi-as ${C_FLAGS} -g startup.s -o startup.o
	${ARM_TOOLCHAIN}/arm-none-eabi-gcc -c ${C_FLAGS} -g test.c -o test.o
	${ARM_TOOLCHAIN}/arm-none-eabi-ld -T test.ld test.o startup.o -o test.elf
	${ARM_TOOLCHAIN}/arm-none-eabi-objcopy -O binary test.elf test.bin

clean:
	rm startup.o
	rm test.o
	rm test.elf
	rm test.bin

run:
	${QEMU} --board NUCLEO-F411RE --mcu STM32F411RE --verbose -s -S --nographic -d unimp,guest_errors

